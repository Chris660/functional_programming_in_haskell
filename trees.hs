-- Functional Programming in Haskell 3.10

data Tree = Leaf | Node Int Tree Tree deriving Show

--let x = Node 3 (Node 2 Leaf Leaf) (Node 7 (Node 5 Leaf Leaf) Leaf)

depth :: Tree -> Int
depth Leaf = 0
depth (Node _ left right) = 1 + max (depth left) (depth right)

treeSum :: Tree -> Int
treeSum Leaf = 0
treeSum (Node val left right) = val + (treeSum left) + (treeSum right)

isSorted :: Tree -> Bool
isSorted tree = isSorted' tree minBound maxBound

isSorted' :: Tree -> Int -> Int -> Bool
isSorted' Leaf _ _ = True
isSorted' (Node val left right) lower upper =
    let
        inBounds = val >= lower && val <= upper
        leftSorted = isSorted' left lower val
        rightSorted = isSorted' right val upper
    in
        inBounds && leftSorted && rightSorted

addNewMax :: Tree -> Int -> Tree
addNewMax Leaf newVal = Node newVal Leaf Leaf
addNewMax (Node val left right) newVal =
    Node val left (addNewMax right newVal)

insert :: Tree -> Int -> Tree
insert Leaf val = Node val Leaf Leaf
insert (Node nval left right) val
    | val <= nval = Node nval (insert left val) right
    | otherwise   = Node nval left (insert right val)

treeToList :: Tree -> [Int]
treeToList Leaf = []
treeToList (Node val left right) =
    (treeToList left) ++ (val : treeToList right)

treeToList2 :: Tree -> [Int]
treeToList2 t = treeToList2' t []

treeToList2' :: Tree -> [Int] -> [Int]
treeToList2' Leaf iis = iis
treeToList2' (Node val left right) iis =
    let iisRight = val : (treeToList2' right iis)
    in  treeToList2' left iisRight

