module ShowParser (parseShow) where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Language

-- PT provides a number of standard parser generators that accept
-- a lexer as an argument.  We can creaye a lexer by calling
-- PT.makeTokenParser.
import qualified Text.ParserCombinators.Parsec.Token as PT

-- Parse 'show' output and translate to XML
parseShow :: String -> String
parseShow str = xmlHeader ++ (run_Parser showParser str)

-- Applies a parser function to an input string.
run_Parser :: Parser a -> String -> a
run_Parser p input = case parse p "" input of
    Left err -> error $ "parse error at: " ++ (show err)
    Right val -> val

-- The main parser
showParser :: Parser String
showParser =
    listParser       <|>
    tupleParser      <|>
    try recordParser <|>
    adtParser        <|>
    number           <|>
    quotedString     <?> "parse error"

-- Create a lexer with an "empty" language definition.
-- emptyDef is defined in the Language module.
lexer = PT.makeTokenParser emptyDef

-- Instantiate our parsers.
parens          = PT.parens lexer
brackets        = PT.brackets lexer
braces          = PT.braces lexer
commaSep        = PT.commaSep lexer
whiteSpace      = PT.whiteSpace lexer
symbol          = PT.symbol lexer
integer         = PT.integer lexer
identifier      = PT.identifier lexer
stringLiteral   = PT.stringLiteral lexer

-- The XML file header
xmlHeader =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

-- Some helpers for outputting tags.
otag t = "<" ++ t ++ ">" -- open t
ctag t = "</" ++ t ++ ">" -- close t
tag t v = concat [otag t, v, ctag t]

-- format key=value pair
kv :: (String, String) -> String
kv (key, value) = key ++ "=\"" ++ value ++ "\""

-- tag with attributes: <tag attr1="..." attr2="...">
atag :: String -> [(String, String)] -> String -> String 
atag t attrs v = concat [
    otag (unwords $ [t] ++ (map kv attrs)), v, ctag t ]

-- join a list of strings with newlines
-- "lines s" converts a string into a list of lines,
-- stripping the '\n' chars - "unlines" does the reverse.
joinNL :: [String] -> String
joinNL strings = unlines strings

-- Lists
-- =====
-- input: [ ..., ..., ... ]
-- output:
--   <list>
--     <list-elt>...</list-elt>
--     ...
--   </list>
listParser = do
    ls <- brackets $ commaSep showParser
    return $ tag "list" $ joinNL $ map (tag "list-elt") ls

-- Tuples
-- ======
-- input: (..., ...)
-- output:
--   <tuple>
--     <tuple-elt>...</tuple-elt>
--     ...
--   </tuple>
tupleParser = do
    ls <- parens $ commaSep showParser
    return $ tag "tuple" $ unwords $ map (tag "tuple-elt") ls

-- Records
-- =======
-- input: Rec { k=v, ... }
-- output:
--   <record>
--     <elt key=k>v</elt>
--     ...
--   </tuple>
recordParser = do
    ti <- typeIdentifier
    ls <- braces $ commaSep kvParser
    return $ atag "record" [("name", ti)] (joinNL ls)

kvParser = do
    k <- identifier
    symbol "="
    t <- showParser
    return $ atag "elt" [("key", k)] t

typeIdentifier = do
    fst <- oneOf ['A' .. 'Z']
    rest <- many alphaNum
    whiteSpace
    return $ (fst : rest)

-- ADTs
-- ====
-- input:  Label
-- output: <adt>Label</adt>
adtParser = do
    ti <- typeIdentifier
    return $ tag "adt" ti

-- Quoted strings, and numbers
-- ===========================
quotedString = do
    s <- stringLiteral
    return $ "\"" ++ s ++ "\""

number = do
    n <- integer
    return $ show n

