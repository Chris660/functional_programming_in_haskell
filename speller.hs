{-
Functional Programming in Haskell 3.7

Are you familiar with children’s alphabetical spelling books?
They say things like: a is for apple, b is for baby, and c is for cat

Now that you know about Haskell list functions, you can develop a
function to generate the text for a spelling book, given a list of words.

Suppose the function is called speller then it should have the following type:

    speller :: [[Char]] -> [Char]

recalling that a list of Char elements is a String in Haskell.

Typical executions of the speller function would look like this:

speller ["abacus"] -- > "a is for abacus"
speller [] -- > ""
speller ["apple", "banana", "coconut"] 
 -- > "a is for apple, b is for banana, and c is for coconut"
speller ["whisky", "x-ray"]
 -- > "w is for whisky, and x is for x-ray"
-}

-- abc "apple" == "a is for apple"
abc :: [Char] -> [Char]
abc (c : cs) = (c : " is for ") ++ (c : cs)

-- Fold a list of words into a comma separated "abc" style
-- string.  This is the bulk of the exercise, and foldr was
-- chosen so that the last word (identified by an empty
-- accumulator) may be given special treatment.
-- This implementation also ignores empty strings in the
-- word list, so for example:
--   speller ["apple", "banana", "", "coconut", ""] ==
--     "a is for apple, b is for banana, and c is for coconut"
-- TODO: list concatenation is convenient, but not very efficient.
abcJoin :: [[Char]] -> [Char]
abcJoin words = foldr join "" words
    where
        join []   acc = acc
        join word []  = "and " ++ (abc word) -- last word
        join word acc = (abc word) ++ ", " ++ acc

-- The speller function as documented in the exercise.
-- Single element word lists are handled directly as a
-- special case to avoid abcJoin adding an "and" prefix.
speller :: [[Char]] -> [Char]
speller [] = ""
speller [word] = abc(word)
speller words  = abcJoin words
