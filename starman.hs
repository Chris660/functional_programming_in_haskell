-- starman.hs - Functional Programming in Haskell week 2.11

import System.Random

readDict :: [String]
readDict = [
    "private", "regret", "governor", "ill-fated", "horn", "capricious", "possess", "simplistic", "snobbish", "careless",
    "descriptive", "reflect", "large", "veil", "head", "succeed", "truck", "stove", "open", "addition", "concentrate",
    "ubiquitous", "splendid", "humorous", "handsomely", "disgusting", "spooky", "fix", "poised", "engine", "observation",
    "day", "cake", "elfin", "saw", "typical", "worry", "dock", "battle", "parsimonious", "house", "dangerous", "reach",
    "imminent", "drown", "finicky", "mice", "cold", "steep", "flower", "tooth", "measure", "second-hand", "wasteful",
    "tart", "damaged", "maddening", "squirrel", "feeble", "notebook", "lettuce", "float", "lush", "charge", "sore",
    "attack", "arrange", "medical", "tomatoes", "entertain", "cute", "group", "reproduce", "deserve", "guiltless", "corn",
    "shivering", "unit", "naive", "garrulous", "release", "fairies", "divergent", "lonely", "substantial", "special",
    "grieving", "chess", "judicious", "sand", "shiny", "imperfect", "invent", "beautiful", "coil", "serious", "pump",
    "pipe", "knot", "dizzy", "sweltering", "afford", "summer", "ladybug", "lick", "fierce", "unknown", "yellow", "cool",
    "water", "gentle", "support", "offer", "ghost", "short", "suck", "proud", "wound", "lunch", "steel", "multiply", "tangy",
    "cable", "cultured", "sink", "defiant", "trace", "concern", "oatmeal", "physical", "honey", "lively", "advertisement",
    "unable", "pig", "ruddy", "woozy", "underwear", "size", "wreck", "right", "youthful", "paste", "own", "chubby",
    "puzzling", "befitting", "best", "name", "riddle", "expand", "card", "new", "useful", "control", "shock", "nation",
    "hobbies", "jazzy", "gate", "exultant", "authority", "dreary", "beg", "stir", "unfasten", "magnificent", "trucks",
    "respect", "flood", "box", "stick", "silver", "lame", "eye", "didactic", "secretive", "dramatic", "scared", "high-pitched",
    "good", "repulsive", "produce", "deserted", "wealth", "adjustment", "watery", "grey", "filthy", "fine", "root",
    "brown", "ordinary", "knowledge", "odd", "pour", "towering", "long-term", "rule", "one" ]
--    do
--        content <- readFile "/usr/share/dict/words"
--        let words = lines content
--        return words
--

check :: String -> String -> Char -> (Bool, String)
check word display c =
    (c `elem` word, [if x == c then x
                               else y | (x, y) <- zip word display])


mkguess :: String -> String -> Int -> IO()
mkguess word display lives =
    do
        putStrLn (display ++ " " ++ (take lives (repeat '*')))
        putStr "Enter a character > "
        guess <- getLine
        let (correct, display') = check word display (guess !! 0)
        let lives' = if correct then lives else lives - 1
        turn word display' lives'

turn :: String -> String -> Int -> IO()
turn word display lives =
    do if lives == 0
        then putStrLn "You lose"
        else if word == display
            then putStrLn ("You won with " ++ (show lives) 
                           ++ " lives remaining.")
            else mkguess word display lives

starman :: String -> IO ()
starman word =
    turn word ['-' | x <- word] (length word)

rstarman :: IO ()
rstarman =
    let
        words = readDict
        numWords = length readDict
        index = getRandom()
        word = words !! index
    in
        starman word
