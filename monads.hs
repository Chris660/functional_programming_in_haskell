import Control.Monad

myHead :: [a] -> Maybe a
myHead [] = Nothing
myHead (x:xs) = Just x

myTail :: [a] -> Maybe [a]
myTail [] = Nothing
myTail (x:xs) = Just xs

-- instance Monad Maybe where
--     return = Just
--     Nothing >>= f = Nothing
--     (Just x) >>= f = f x
--     fail _ = Nothing

bar3 :: [a] -> Maybe a
bar3 xs = do
    a <- myTail xs
    b <- myTail a
    myHead b

