data Bright
    = Blue
    | Red
    deriving (Read, Show)

darkBright :: Bright -> Bool
darkBright Blue = True
darkBright Red  = False

lightenBright :: Bright -> Bright
lightenBright Blue = Red
lightenBright Red  = Red

data Pastel
    = Turquoise
    | Magenta
    deriving (Read, Show)

darkPastel :: Pastel -> Bool
darkPastel Turquoise = True
darkPastel Magenta   = False

lightenPastel :: Pastel -> Pastel
lightenPastel Turquoise = Magenta
lightenPastel Magenta   = Magenta

class Colour a where
    dark :: a -> Bool
    lighten :: a -> a

instance Colour Bright where
    dark    = darkBright
    lighten = lightenBright

instance Colour Pastel where
    dark    = darkPastel
    lighten = lightenPastel


data Foo = Bar | Baz
instance Show Foo where
    show Bar = "Foo: Bar"
    show Baz = "Foo: Baz"

