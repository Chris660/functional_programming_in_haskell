holeScore :: Int -> Int -> String
holeScore shots par
    | score < 0 = (show (abs score)) ++ " under par"
    | score > 0 = (show score) ++ " over par"
    | otherwise = "even par"
    where score = shots - par

data Pet = Dog | Cat | Rabbit

hello :: Pet -> String
hello pet = case pet of
    Dog -> "woof!"
    Cat -> "mieouw"
    Rabbit -> "twitch, twitch"


