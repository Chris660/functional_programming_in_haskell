import Data.Char

-- is this a letter?
shouldCipher :: Char -> Bool
shouldCipher c = isLetter c && isAscii c

-- Caesar cipher with shift n
cipherChar :: Int -> Char -> Char
cipherChar n c
    | shouldCipher c = rotateChar n c
    | otherwise      = c

-- Rotates an upper case character `shift` places.
rotateChar :: Int -> Char -> Char
rotateChar shift c =
    let
        base = ord (if isUpper c then 'A' else 'a')
        c_idx = ord c - base
        new_idx = (c_idx + shift) `mod` 26
    in
        chr (base + new_idx)

-- Creates a Caesar's cipher of `shift` chars.
cipher :: Int -> [Char] -> [Char]
cipher shift plaintext = map (cipherChar shift) plaintext

decipher :: Int -> [Char] -> [Char]
decipher shift ciphertext = cipher (-shift) ciphertext


-- QuickCheck test cases
checkCaesar :: Int -> [Char] -> Bool
checkCaesar n s = (decipher n (cipher n s)) == s

