-- Basic list processing functions.
-- Functional programming in Haskell 3.2

r_length :: [a] -> Int
r_length [] = 0
r_length (x : xs) = 1 + r_length xs

r_filter :: (a -> Bool) -> [a] -> [a]
r_filter _ [] = []
r_filter pred (x : xs) =
    if pred x then x : (r_filter pred xs)
    else (r_filter pred xs)

r_map :: (a -> b) -> [a] -> [b]
r_map _ [] = []
r_map fun (x : xs) = (fun x) : r_map fun xs

-- foldl (+) z [x0,x1,x2]  -- > ((z + x0) + x1) + x2
-- foldr (+) z [x0,x1,x2]  -- > x0 + (x1 + (x2 + z))
r_foldl :: (b -> a -> b) -> b -> [a] -> b
r_foldl _ acc [] = acc
r_foldl f acc (x : xs) = r_foldl f (f acc x) xs

r_foldr :: (a -> b -> b) -> b -> [a] -> b
-- r_foldr _ acc [] = acc
-- r_foldr f acc (x : xs) = f (r_foldr f acc xs) x
r_foldr k z = go
    where
        go [] = z
        go (x : xs) = x `k` go xs
